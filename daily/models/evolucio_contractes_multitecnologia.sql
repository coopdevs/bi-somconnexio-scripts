
{{ config(
    materialized='table',
  )
}}

select data, ost."name"  as tecnologia
,count(case when comparteix='SÍ' then 1 else null end) as contr_multitecnologia
,count(case when comparteix='SÍ' then null else 1 end) as contr_nomultitecnologia
,count(case when comparteix='SÍ' then 1 else null end)+count(case when comparteix='SÍ' then null else 1 end)  as contr_totals
,count(case when comparteix='SÍ' then 1 else null end)::numeric/(count(case when comparteix='SÍ' then 1 else null end)+count(case when comparteix='SÍ' then null else 1 end))  as contr_perc_multitecnologia
,count(case when comparteix='SÍ' then null else 1 end)::numeric/(count(case when comparteix='SÍ' then 1 else null end)+count(case when comparteix='SÍ' then null else 1 end))  as contr_perc_no_multitecnologia
,count(distinct case when comparteix='SÍ' then partner_id  else null end) as persones_multitecnologia
,count(distinct case when comparteix='SÍ' then null else partner_id end) as persones_no_multitecnologia
,count(distinct case when comparteix='SÍ' then partner_id else null end)+count(case when comparteix='SÍ' then null else partner_id end)  as persones_totals
,count(distinct case when comparteix='SÍ' then partner_id else null end)::numeric/(count (distinct case when comparteix='SÍ' then partner_id else null end)+count(distinct case when comparteix='SÍ' then null else partner_id end))  as persones_perc_multitecnologia
,count(distinct case when comparteix='SÍ' then null else partner_id end)::numeric/(count (distinct case when comparteix='SÍ' then partner_id else null end)+count(distinct case when comparteix='SÍ' then null else partner_id end))  as persones_perc_no_multitecnologia
from (
	select d.data, m.partner_id, m."name", m.service_technology_id, case when a.partner_id is null then 'NO' else 'SÍ' end as comparteix
	from odoo_contract_contract m
		join data d on d.data between m.date_start  and coalesce(m.terminate_date, '29991231') and date_part('day', d.data)=1
		left join v_partner_technology_data a on m.partner_id =a.partner_id
			and a.service_technology_id<>m.service_technology_id  and a.service_technology_id in (1,3)
			and a.data=d.data
		where  m.service_technology_id in (1,3)
) a
join odoo_service_technology ost on ost.id =a.service_technology_id
where data >= '20210101' and data<date_trunc('year', current_date)+ interval '1 year'
and service_technology_id in (1,3)
group by data, ost."name"
order by 1,2