
{{ config(materialized='incremental', unique_key='data') }}

SELECT CAST(s.create_date AS date) AS data, count(*) AS altes_socia
FROM public.odoo_subscription_request s
	LEFT JOIN public.odoo_res_partner p ON s.partner_id = p.id
WHERE (s.type = 'new' OR s.type = 'subscription')
  AND (s.state = 'done' OR s.state = 'draft' OR s.state = 'paid')
GROUP BY CAST(s.create_date AS date)
