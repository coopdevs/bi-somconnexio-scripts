{{ config(materialized='table') }}

select
id,data_extracte,extracte,balance_start,balance_end,data_linia,linia,import_linia,move_name,partner,tipus_moviment,ratio,import_linia_proratejat,account_name,
case when partner is null and account_name is not null and tipus_moviment='Entrada' then 'Cobros Factura mantenimiento'
when partner is not null and tipus_moviment='Entrada' then 'Cobro por proyectos Clientes'
when partner is null and account_name is null and tipus_moviment='Entrada' then 'Otros cobros'
when account_name='Remuneraciones pendientes de pago' and tipus_moviment='Sortida' then 'PERSONAL'
when partner is null and linia='PAGAMENT HISENDA' and tipus_moviment='Sortida' then 'PAGO DE IMPUESTOS'
when partner = 'Agencia Estatal de Administración Tributaria (AEAT)' and linia='PAGAMENT HISENDA' and tipus_moviment='Sortida' then 'PAGO DE IMPUESTOS'
when partner is null and (linia ilike 'com.%' or linia ilike 'COMISSIÓ%') and tipus_moviment='Sortida' then 'GASTOS POR PARTIDAS'
when partner = 'Coopdevs Treball SCCL'  and linia ='R/ TGSS. COTIZACION 001 REGIMEN GEN ERA' and tipus_moviment='Sortida' then 'PERSONAL'
when partner is not null and tipus_moviment='Sortida' then 'CLIENTE'
else 'CLIENTE' end as lvl1
,
case when partner is null and account_name is not null and tipus_moviment='Entrada' then ' '
when partner is not null and tipus_moviment='Entrada' then partner
when partner is null and account_name is null and tipus_moviment='Entrada' then ' '
when account_name='Remuneraciones pendientes de pago' and tipus_moviment='Sortida' then 'Pagos de sueldos'
when partner is null and linia='PAGAMENT HISENDA' and tipus_moviment='Sortida' then 'IRPF'
when partner is null and (linia ilike 'com.%' or linia ilike 'COMISSIÓ%') and tipus_moviment='Sortida' then 'Comisiones'
when partner = 'Agencia Estatal de Administración Tributaria (AEAT)' and linia='PAGAMENT HISENDA' and tipus_moviment='Sortida' then 'IVA'
when partner = 'Coopdevs Treball SCCL'  and linia ='R/ TGSS. COTIZACION 001 REGIMEN GEN ERA' and tipus_moviment='Sortida' then 'Pagos a la seg. social'
when partner is not null and tipus_moviment='Sortida' then partner
else coalesce(partner, linia) end as lvl2
from (
select a.id,
          a.data_extracte,
          a."name" as extracte,
          a.balance_start,
          a.balance_end,
          absl.date as data_linia,
          absl.name as linia,
          absl.amount import_linia,
          absl.move_name,
          coalesce(v.partner_name,rp."name") as partner,
          case
              when absl.amount <0 then 'Sortida'
              else 'Entrada'
          end as tipus_moviment
          ,v.ratio
          ,absl.amount*coalesce(v.ratio,1) as import_linia_proratejat
          , v.account_name
   from
     (select a.id,
             date_trunc('month', max(absl.date)) as data_extracte,
             a."name",
             a.balance_start, a.balance_end
      from odoo_account_bank_statement a
      join odoo_account_bank_statement_line absl on a.id=absl.statement_id
      group by a.balance_start, a.balance_end,
               a.id,
               a."name") a
   join odoo_account_bank_statement_line absl on a.id=absl.statement_id
   left join odoo_res_partner rp on absl.partner_id =rp.id
   left join v_moves_not_bank v on  v.move_amount=abs(absl.amount) and v.journal_type='bank' and (absl.move_name=v.move_name or absl.move_name is null) and move_date=absl.date
 ) a