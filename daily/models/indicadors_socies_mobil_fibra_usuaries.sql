{{ config(materialized='table') }}


select d."data" as data, o."any"::int, o.dia_de_la_setmana
	, date_part('month',d.data)<=date_part('month', date_trunc('month',current_date)-interval '1 month') as mes_tancat
	, date_part('month',d.data)<=date_part('month', date_trunc('month',current_date)-interval '1 month<') and date_part('year',d.data)=date_part('year', date_trunc('month',current_date)-interval '1 month') as mes_tancat_any_actual
	, date_part('day', d.data)=d.dies_mes or d.data=current_date-interval '1 day' as tancament_mes
	, d.data - concat((date_part('year', d.data)-1900)::varchar, ' year')::interval as data_sense_any
	,altes_socia, baixes_socia, altes_netes_socia, projeccio_altes_socia
		, altes_netes_socia-projeccio_altes_socia as diferencia_projeccio_altes_netes_socia
		,sum(altes_netes_socia) over(partition by date_part('year',d."data") order by d."data") as altes_netes_socia_acumulat_anual
		,sum(projeccio_altes_socia) over(partition by date_part('year',d."data") order by d."data") as projeccio_altes_socia_acumulat_anual
		,sum(altes_netes_socia-projeccio_altes_socia) over(partition by date_part('year',d."data") order by d."data") as diferencia_projeccio_altes_netes_socia_acumulat_anual
	,activacions_mobil , baixes_mobil, activacions_mobil-baixes_mobil as altes_netes_mobil, projeccio_altes_mobil
		, activacions_mobil-baixes_mobil-projeccio_altes_mobil as diferencia_projeccio_altes_netes_mobil
		,sum(activacions_mobil-baixes_mobil) over(partition by date_part('year',d."data") order by d."data") as altes_netes_mobil_acumulat_anual
		,sum(projeccio_altes_mobil) over(partition by date_part('year',d."data") order by d."data") as projeccio_altes_mobil_acumulat_anual
		,sum(activacions_mobil-baixes_mobil-projeccio_altes_mobil ) over(partition by date_part('year',d."data") order by d."data") as diferencia_projeccio_altes_netes_mobil_acumulat_anual
		,coalesce(tm.peticions_contracte,0) as peticions_contracte_mobil
	,activacions_fibra , baixes_fibra, activacions_fibra-baixes_fibra as altes_netes_fibra, projeccio_altes_fibra
		, activacions_fibra-baixes_fibra-projeccio_altes_fibra as diferencia_projeccio_altes_netes_fibra
		,sum(activacions_fibra-baixes_fibra) over(partition by date_part('year',d."data") order by d."data") as altes_netes_fibra_acumulat_anual
		,sum(projeccio_altes_fibra) over(partition by date_part('year',d."data") order by d."data") as projeccio_altes_fibra_acumulat_anual
		,sum(activacions_fibra-baixes_fibra-projeccio_altes_fibra ) over(partition by date_part('year',d."data") order by d."data") as diferencia_projeccio_altes_netes_fibra_acumulat_anual
		,coalesce(tf.peticions_contracte,0) as peticions_contracte_fibra
	,activacions_adsl , baixes_adsl, activacions_adsl-baixes_adsl as altes_netes_adsl
		,sum(activacions_adsl-baixes_adsl) over(partition by date_part('year',d."data") order by d."data") as altes_netes_adsl_acumulat_anual
		,coalesce(ta.peticions_contracte,0) as peticions_contracte_adsl
	,activacions_4g , baixes_4g, activacions_4g-baixes_4g as altes_netes_4g
		,sum(activacions_4g-baixes_4g) over(partition by date_part('year',d."data") order by d."data") as altes_netes_4g_acumulat_anual
	,o.pet_contracte as peticions_contracte_total
	,o.altes_apadrinades, o.baixes_apadrinades, o.altes_apadrinades-o.baixes_apadrinades as altes_netes_apadrinades
	,o.altes_intercooperacio, o.baixes_intercooperacio, o.altes_intercooperacio-o.baixes_intercooperacio as altes_netes_intercooperacio
	,o.altes_socia+o.altes_apadrinades+o.altes_intercooperacio as altes_usuaries
		,o.baixes_socia+o.baixes_apadrinades+o.baixes_intercooperacio as baixes_usuaries
		,(o.altes_socia+o.altes_apadrinades+o.altes_intercooperacio)-(o.baixes_socia+o.baixes_apadrinades+o.baixes_intercooperacio) as altes_netes_usuaries
		,o.projeccio_altes_usuaria
		,(o.altes_socia+o.altes_apadrinades+o.altes_intercooperacio)-(o.baixes_socia+o.baixes_apadrinades+o.baixes_intercooperacio)-o.projeccio_altes_usuaria as diferencia_projeccio_altes_netes_usuaries
		,sum((o.altes_socia+o.altes_apadrinades+o.altes_intercooperacio)-(o.baixes_socia+o.baixes_apadrinades+o.baixes_intercooperacio) ) over(partition by date_part('year',d."data") order by d."data") as altes_netes_usuaries_acumulat_anual
		,sum(o.projeccio_altes_usuaria ) over(partition by date_part('year',d."data") order by d."data") as projeccio_altes_usuaria_acumulat_anual
		,sum((o.altes_socia+o.altes_apadrinades+o.altes_intercooperacio)-(o.baixes_socia+o.baixes_apadrinades+o.baixes_intercooperacio)-o.projeccio_altes_usuaria  ) over(partition by date_part('year',d."data") order by d."data") as diferencia_projeccio_altes_netes_usuaries_acumulat_anual
from data d
	join odoo_indicadors_socies_mobil_fibra o on o.data=d."data"
	left join v_peticions_contracte_tecnologia tm on tm.create_date =d.data and tm.tecnologia='Mòbil'
	left join v_peticions_contracte_tecnologia tf on tf.create_date =d.data and tf.tecnologia='Fibra'
	left join v_peticions_contracte_tecnologia ta on ta.create_date =d.data and ta.tecnologia='ADSL'
where d.data<current_date
	and d.data>='20190101'