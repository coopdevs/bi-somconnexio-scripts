{{ config(materialized='view') }}


SELECT d.data,
o.code,
o.amount_without_tax_rated_transaction,
o.supplier_name,
o.technology_name,
o.producte
FROM {{ ref('v_detall_factures_opencell') }} o
JOIN ( SELECT d_1.data
    FROM public.data d_1
   WHERE date_part('day'::text, d_1.data) = 1::double precision) d ON
    date_trunc('month'::text, o.invoice_date) = d.data
     AND date_trunc('month'::text, o.date_start_contract::timestamp with time zone) <> d.data
     AND date_trunc('month'::text, coalesce(o.date_end_contract::timestamp with time zone, '99991231')) <> d.data
