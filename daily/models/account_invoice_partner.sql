{{ config(materialized='table') }}

select ai.date_invoice as effective_date,  aml.date as sc_cooperator_end_date, rp.id
from odoo_account_invoice ai
    join odoo_account_invoice_account_move_line_rel r on r.account_invoice_id =ai.id
    join odoo_account_move_line aml on r.account_move_line_id =aml.id
    join odoo_subscription_request sr on ai.subscription_request =sr.id
    join odoo_res_partner rp on sr.partner_id =rp.id
where 1=1
    and rp.cooperator_register_number is null
    and payment_type='split'
    and sr.state<>'done'
    and balance =-100
union all
select ai.date_invoice as effective_date,  '99991231' as sc_cooperator_end_date, rp.id
from odoo_account_invoice ai
    join odoo_subscription_request sr on ai.subscription_request =sr.id
    join odoo_res_partner rp on sr.partner_id =rp.id
where 1=1
    and rp.cooperator_register_number is null
    and payment_type='split'
    and sr.state='done'
