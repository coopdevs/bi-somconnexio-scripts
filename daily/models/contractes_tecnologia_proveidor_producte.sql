{{ config(materialized='table') }}


select "data" as data,technology_name,supplier_name,producte
	,sum(altes) as altes,sum(baixes) as baixes
	,sum(altes)-sum(baixes) as altes_netes
	,sum(sum(altes)) over (partition by technology_name,supplier_name,producte order by (data)) as altes_acumulades
	,sum(sum(baixes)) over (partition by technology_name,supplier_name,producte order by (data)) as baixes_acumulades
	,sum(sum(altes)) over (partition by technology_name,supplier_name,producte order by (data))-sum(sum(baixes)) over (partition by technology_name,supplier_name,producte order by (data))  as contractes_acumulats
	,sum(import_contractes) as import_contractes
	,sum(sum(import_contractes)) over (partition by technology_name,supplier_name,producte order by (data)) as import_contractes_acumulat
	,sum(projeccio) as projeccio
	,sum(sum(projeccio)) over (partition by technology_name,supplier_name,producte order by (data)) as projeccio_acumulada_sense_dades_inicials
	,sum(sum(projeccio_inicial)) over (partition by technology_name,supplier_name,producte order by (data)) + sum(sum(projeccio)) over (partition by technology_name,supplier_name,producte order by (data)) as projeccio_acumulada
from (
	select occ.date_start as data, st.name AS technology_name, ss.name AS supplier_name,pp.default_code producte
		,count(*) as altes
		,0 as baixes
	    ,count(*)*avg(coalesce(ppi.fixed_price,0))/max(d.dies_mes)  as import_contractes
	    ,0 as projeccio
	    ,0 as projeccio_inicial
	from odoo_contract_contract occ
		JOIN odoo_service_technology st ON occ.service_technology_id = st.id
	    JOIN odoo_service_supplier ss ON occ.service_supplier_id = ss.id
	    join odoo_contract_line cl on occ.current_tariff_contract_line =cl.id
		join odoo_product_product pp on cl.product_id =pp.id
		join data d on occ.date_start =d."data"
		left join
	     (select ppi.product_id,
	             ppi.fixed_price
	      from odoo_product_pricelist_item ppi
	      join odoo_product_pricelist pr on ppi.pricelist_id =pr.id
	      where pr.code='0IVA') ppi on ppi.product_id =pp.id
	group by occ.date_start, st.name, ss.name,pp.default_code
	union all
	select occ.date_end, st.name AS technology_name, ss.name AS supplier_name,pp.default_code producte
		,0 as altes
		,count(*) as baixes
	    ,0 as import_contractes
	    ,0 as projeccio
	    ,0 as projeccio_inicial
	from odoo_contract_contract occ
		JOIN odoo_service_technology st ON occ.service_technology_id = st.id
	    JOIN odoo_service_supplier ss ON occ.service_supplier_id = ss.id
	    join odoo_contract_line cl on occ.current_tariff_contract_line =cl.id
		join odoo_product_product pp on cl.product_id =pp.id
	where occ.date_end is not null
	group by occ.date_end, st.name, ss.name,pp.default_code
	union all
	SELECT data.data, 'Mobile' as technology_name ,'Projecció' as supplier_name, 'Projecció' as producte
		, 0,0,0, SUM(p.altes_netes_mobil)/cast(dies_mes as float) AS projeccio, 0 as projeccio_inicial
	FROM projeccions p
		join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
	WHERE data.data>='20220101'
	GROUP BY data.data, dies_mes
	union all
	SELECT data.data, 'Fiber'  as technology_name ,'Projecció' as supplier_name, 'Projecció' as producte
		, 0,0,0, SUM(p.altes_netes_BA)/cast(dies_mes as float) AS projeccio,0 as projeccio_inicial
	FROM projeccions p
		join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
	WHERE data.data>='20220101'
	GROUP BY data.data, dies_mes
	union all
	select data, 'Fiber' ,'Projecció','Projecció' as producte
		,0,0,0,0, acum_contractes_BA
	from (select data-interval '1 day' as data,  acum_contractes_BA, row_number() over( order by data) as rn
	from projeccions p
	) a
	where rn=1
	union all
	select data, 'Mobile' ,'Projecció','Projecció' as producte
		,0,0,0,0, acumulat_contractes_mobil
	from (select data-interval '1 day' as data,  acumulat_contractes_mobil, row_number() over( order by data) as rn
	from projeccions p
	) a
	where rn=1
) a
group by "data",technology_name,supplier_name,producte
order by "data",technology_name,supplier_name,producte