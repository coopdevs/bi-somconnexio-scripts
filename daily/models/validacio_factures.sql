
{{ config(materialized='table') }}

select
case when o.code is null then 'FALTA ODOO' else 'CORRECTE' end as flag_existeix_odoo
,case when c.subscriptioncode is null then 'FALTA OPENCELL' else 'CORRECTE' end as flag_existeix_opencell
,case when coalesce(o.default_code,'xx')<>c.servicecode then 'PRODUCTE DIFERENT' else 'CORRECTE' end as flag_validacio_producte
,case when coalesce(o.date_end, '99991231')<>coalesce(c.Serviceterminationdate,'99991231') then 'DATA FI DIFERENT' else 'CORRECTE' end as flag_validacio_data_fi
,case when o.date_start<>c.Serviceactivationdate then 'DATA INICI DIFERENT' else 'CORRECTE' end as flag_validacio_data_inici
,default_code as codi_producte_odoo, code as codi_contracte_odoo, date_start as date_start_odoo, date_end as date_end_odoo, "name" as telefon_odoo,
subscriptioncode as codi_contracte_opencell, "subscription" as telefon_opencell, subscriptionstatus as subscription_status_opencell,bsiid,subscriptionid,servicecode as codi_producte_opencell,
serviceactivationdate as date_start_opencell ,serviceterminationdate as date_end_opencell, termination_date,id,useraccountcode,billing_cycle,trading_country_id,trading_language_id,billing_account_id,billingcyclecode,pais,idioma,subsbillingcycle,ebilling,buadesc
from opencell_contract_lines c
	full join odoo_contract_lines o on c.Subscriptioncode=o.code
		and coalesce(o.date_end, '99991231')>=c.Serviceactivationdate
		and o.date_start<=coalesce(c.Serviceterminationdate,'99991231')
