{{ config(materialized='table') }}

select occ.date_start, occ.date_end, occ.partner_id, rp.coop_sponsee,  rp.coop_agreement
from odoo_contract_contract occ
	join odoo_res_partner rp on occ.partner_id =rp.id