{{ config(materialized='table') }}


SELECT j.idvisit, j.idsite, j.idvisitor, j.visit_last_action_time, j.location_ip, j.config_os, j.location_city, j.location_country, j.location_region
, j.visitor_returning, j.visitor_count_visits
,c.url , c.server_time
 , g.name as goal_name
from matomo_db_matomo_log_visit j
  join matomo_db_matomo_log_conversion c on  j.idvisit=c.idvisit
  join matomo_db_matomo_goal g on c.idgoal=g.idgoal
where j.idsite=4
union all
select
null, null, null, null, null, null, null, null, null
, null, null
,null , generate_series(date '2020-01-01', current_date , '1 day')
, g.name
from matomo_db_matomo_goal g
where idsite =4
