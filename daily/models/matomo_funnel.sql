{{ config(materialized='table') }}

select idfunnel, funnel_name, pattern, position , step_name, step_pattern
	, final_position
	, max_step_position, max_step_name, max_step_position_name
	, is_funnel_end
	, idvisitor, idvisit
	, min_step, max_step
	, location_browser_lang, config_browser_engine , config_browser_name,  config_browser_version
		, config_device_brand, config_os, config_os_version
	, location_city, location_region, location_country
	, server_time
	,  url
	, tipus
from matomo_db_v_funnel