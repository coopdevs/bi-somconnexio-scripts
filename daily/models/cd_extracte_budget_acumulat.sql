{{ config(materialized='table') }}


select d.data,sum(balance) as balance, tipus, estat, rf, real_calculat
from (
	select date_trunc('month',aml.date_maturity) as data ,  sum(aml.balance) as balance
	, case when aa.internal_type ='payable'  then 'Sortida' else 'Entrada' end as tipus
	, case when aml.full_reconcile_id is null then 'pendent' else 'executat' end as estat
	,case when  aml.date_maturity <current_date then 'REAL VENÇUDA' else 'REAL FUTURA'end as rf
	,'CALCULAT' as real_calculat
	from odoo_account_move am
	 join odoo_account_move_line aml on am.id=aml.move_id
	 join odoo_account_account aa on aml.account_id =aa.id
	where aa.internal_type  in ('payable', 'receivable')
	and date_trunc('month',aml.date_maturity)>( select date_trunc('month',max(date))  from odoo_account_bank_statement_line)
	group by  date_trunc('month',aml.date_maturity)
	, case when aa.internal_type ='payable'  then 'Sortida' else 'Entrada' end
	, case when aml.full_reconcile_id is null then 'pendent' else 'executat' end
	,case when  aml.date_maturity <current_date then 'REAL VENÇUDA' else 'REAL FUTURA'end
) a
, data d
where date_part('day',d.data) =1
and d.data>((select date_trunc('month', max(absl.date)) from odoo_account_bank_statement_line absl))
and a.data<=d.data
group by d.data, tipus, estat, rf, real_calculat
union all
select d.data,sum(balance) as balance, tipus, estat, rf, real_calculat
from (
	select date_trunc('month',fc."date") as data, sum(fc.balance) as balance
	, case when fc.balance<0  then 'Sortida' else 'Entrada' end as tipus
	,'pendent' as estat
	,case when  fc."date" <current_date then 'PRESSUPOST VENÇUT' else 'PRESSUPOST FUTUR'end as rf, 'CALCULAT' as real_calculat
	from odoo_mis_cash_flow_forecast_line fc
	where date_trunc('month',fc.date)>( select date_Trunc('month',max(date))  from odoo_account_bank_statement_line)
	group by date_trunc('month',fc."date")
	, case when fc.balance<0  then 'Sortida' else 'Entrada' end
	,case when  fc."date" <current_date then 'PRESSUPOST VENÇUT' else 'PRESSUPOST FUTUR'end
	order by 1
) a
, data d
where date_part('day',d.data) =1
and d.data>((select date_trunc('month', max(absl.date)) from odoo_account_bank_statement_line absl))
and a.data<=d.data
group by d.data, tipus, estat, rf, real_calculat
union all
select date_trunc('month', max(absl.date)) as data_extracte,
 	a.balance_end
 	,'BANK STATEMENT', 'BANK STATEMENT', 'BANK STATEMENT','REAL'
from odoo_account_bank_statement a
  join odoo_account_bank_statement_line absl on a.id=absl.statement_id
group by a.balance_start, a.balance_end, a."name"
union all
select distinct d.data as data_extracte,
 	a.balance_end
 	,'BANK STATEMENT', 'BANK STATEMENT', 'BANK STATEMENT','CALCULAT'
from odoo_account_bank_statement a
  join odoo_account_bank_statement_line absl on a.id=absl.statement_id
  , data d
where date_trunc('month', absl.date) = ((select date_trunc('month', max(absl.date)) from odoo_account_bank_statement_line absl))
and date_part('day',d.data) =1
and d.data>=((select date_trunc('month', max(absl.date)) from odoo_account_bank_statement_line absl))