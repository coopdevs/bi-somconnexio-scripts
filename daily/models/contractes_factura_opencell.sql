
{{ config(materialized='table') }}

SELECT c.id,c.code,c.date_start,c.date_end,c.is_terminated,c.terminate_date,c.service_technology_id,c.service_supplier_id,c.phone_number,
 a.invoice_date as invoice_date, a.invoice_number, a.amount_without_tax
from odoo_contract_contract c
left join(
  select *
  from (
  select s.code, i.invoice_date as invoice_date, i.invoice_number, i.amount_without_tax, row_number() over(partition by s.code order by i.invoice_date desc) as rn
  from opencell_billing_subscription s
  join opencell_billing_rated_transaction rt on s.id=rt.subscription_id
  join opencell_billing_invoice i on rt.invoice_id = i.id
  where start_date is not null
  ) s
  where s.rn=1
) a on c.code=a.code
