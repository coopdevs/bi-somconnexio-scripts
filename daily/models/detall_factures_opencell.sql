
{{ config(
    materialized='table',
    indexes=[
    {'columns': ['code']},
    ]
  )
}}

select s.code, i.invoice_date as invoice_date, i.invoice_number, i.amount_without_tax as amount_without_tax_invoice,
	rt.amount_without_tax as amount_without_tax_rated_transaction, rt.code as code_rated_transaction, rt.quantity as quantity_rated_transaction
from opencell_billing_subscription s
	join opencell_billing_rated_transaction rt on s.id=rt.subscription_id
	join opencell_billing_invoice i on rt.invoice_id = i.id
where rt.amount_without_tax<>0
	and i.invoice_date>='20210101'
